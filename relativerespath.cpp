#include "bbconst.h"
#include "ResourcePath.hpp"

std::string resourcePath() {
  return std::string(".") + BB_DIRSEP;
}
